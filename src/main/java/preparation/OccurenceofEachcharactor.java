package preparation;

import java.util.HashMap;
import java.util.Map;

public class OccurenceofEachcharactor {

	
	public static void main(String[] args) {
		
		String str = "welcome to automation";
		
		
				HashMap<Character, Integer> charCountMap = new HashMap<Character, Integer>();
				
				char[] ch = str.toCharArray();
				
				for (char c : ch) {
					if(charCountMap.containsKey(c)) {
						charCountMap.put(c, charCountMap.get(c)+1);
					}
					else {
						charCountMap.put(c, 1);
					}
				}
				
				for(Map.Entry entry : charCountMap.entrySet()) {
					System.out.println(entry.getKey()+"-"+entry.getValue());
					
				}
		
	}
}
