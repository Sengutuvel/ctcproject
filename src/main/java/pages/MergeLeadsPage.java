package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.Projectmethods;

public class MergeLeadsPage extends Projectmethods {

	public MergeLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH,using="//input[@id='partyIdFrom']/following-sibling::a/img") WebElement eleidfrom;
	@FindBy(how = How.XPATH,using="//input[@id='partyIdTo']/following-sibling::a/img") WebElement eleidTo;
	@FindBy(how = How.LINK_TEXT,using="Merge") WebElement eleMerge;

	public FindLeadsPage clickPartyIdFromimg() {
		click(eleidfrom);
		switchToWindow(1);
		return new FindLeadsPage();
	}
	public FindLeadsPage clickPartyIdToimg() {
		click(eleidTo);
		switchToWindow(1);
		return new FindLeadsPage();
	}
	
	public ViewLeadPage clickMergeButton() {
		
		clickWithOutSnap(eleMerge);
		return new ViewLeadPage();
	}
}
