package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.Projectmethods;

public class ViewLeadPage extends Projectmethods {

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID,using="viewLead_firstName_sp") WebElement elefirstName;

	
	

	public ViewLeadPage verifyFirstName() {
		verifyExactText(elefirstName, "sengutuvel");
		return this;
	}




	}

