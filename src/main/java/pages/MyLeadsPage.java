package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.Projectmethods;

public class MyLeadsPage extends Projectmethods {

	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT,using="Create Lead") WebElement eleCreateLead;
	@FindBy(how = How.LINK_TEXT,using="Merge Leads") WebElement eleMergeLead;
@And("click the create lead button")
	public CreateLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	public MergeLeadsPage clickMergeLead() {
		click(eleMergeLead);
		return new MergeLeadsPage();
	}
}
