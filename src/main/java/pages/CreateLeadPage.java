package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import wdMethods.Projectmethods;

public class CreateLeadPage extends Projectmethods {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCpName;
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement elefname;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement elelname;
	@FindBy(how = How.ID,using="createLeadForm_primaryPhoneNumber") WebElement elepnum;
	@FindBy(how = How.ID, using = "createLeadForm_primaryEmail") WebElement elemail;
	@FindBy(how = How.XPATH,using="//input[@value='Create Lead']") WebElement elecreatelead;
	
	
@And("enter the company name as (.*)")
	public CreateLeadPage entercompanyName(String cpname) {
		type(eleCpName, cpname);
		return this;
	}
@And("enter the first name as (.*)")
	public CreateLeadPage entefirstName(String fname) {
		type(elefname, fname);
		return this;
	}
@And("enter the second name as (.*)")
	public CreateLeadPage entelastName(String lname) {
		type(elelname, lname);
		return this;
	}

	public CreateLeadPage entePNumber(String ph) {
		type(elepnum, ph);
		return this;
	}

	public CreateLeadPage enteEmail(String mail) {
		type(elemail, mail);
		return this;
	}
	@When("click the createleaad button")
	public ViewLeadPage clickCreateLeadbutton() {
		click(elecreatelead);
		return new ViewLeadPage();
	}
}
