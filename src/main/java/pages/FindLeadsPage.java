package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.Projectmethods;

public class FindLeadsPage extends Projectmethods {

	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.NAME,using = "firstName") WebElement eleFirstName;
	@FindBy(how=How.XPATH,using = "//button[text()='Find Leads']") WebElement eleFindLead;
	@FindBy(how=How.XPATH,using = "(//a[@class='linktext'])[1]") WebElement eleFirstLeadId;
	
	public FindLeadsPage enterFirstName(String firstname) {
		type(eleFirstName, firstname);
		return this;
	}
	public FindLeadsPage clickFindLeadButton() throws InterruptedException {
		clickWithOutSnap(eleFindLead);
		Thread.sleep(3000);
		return this;
	}
	
	public MergeLeadsPage clickFirstLeadIdSelection() {
		clickWithOutSnap(eleFirstLeadId);
		switchToWindow(0);
		return new MergeLeadsPage();
	}
	
	
}
