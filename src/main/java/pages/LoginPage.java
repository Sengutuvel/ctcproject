package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.Projectmethods;

public class LoginPage extends Projectmethods {
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "username") WebElement eleusername;
	@FindBy(how = How.ID, using = "password") WebElement elepassword;
	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit") WebElement elelogin;
	
	@And("enter the username as (.*)")
	public LoginPage enterUserName(String uname) {
		type(eleusername, uname);
		return this;
	}
	
	@And("enter the password as (.*)")
	public LoginPage enterPassword(String pword) {
		type(elepassword, pword);
		return this;
	}
	@And("click the loging button")
	public HomePage clickLogin() {
		click(elelogin);
		return new HomePage();
	}
}
