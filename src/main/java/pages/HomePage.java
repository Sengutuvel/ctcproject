package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.Projectmethods;

public class HomePage extends Projectmethods {

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit") WebElement elelogout;
	@FindBy(how = How.LINK_TEXT,using = "CRM/SFA") WebElement eleCRMF;
	

	public LoginPage clickLogout() {
		click(elelogout);
		return new LoginPage();
	}
	@And("click the CRM link")
	public MyHomePage clickCRMSFA() {
		click(eleCRMF);
		return new MyHomePage();
	}
}
