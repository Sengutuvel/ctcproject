package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.Projectmethods;

public class MyHomePage extends Projectmethods {

	public MyHomePage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT,using="Leads") WebElement eleLeads;
@And("click the leads tab")
	public MyLeadsPage clickLeads() {
		click(eleLeads);
		return new MyLeadsPage();
	}
}
