package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Report;

public class SeMethods extends Report implements WdMethods{
	public int i = 1;
	public static RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//System.out.println("The Browser "+browser+" Launched Successfully");
			reportStep("pass", "The Browser "+browser+" Launched Successfully");
			
		} catch (WebDriverException e) {
			//System.out.println("The Browser "+browser+" not Launched ");
			reportStep("fail", "The Browser "+browser+" not Launched ");
		} finally {
			takeSnap();			
		}
	}
	

	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "name": return driver.findElementByName(locValue);
			case "linkText": return driver.findElementByLinkText(locValue);
			case "tagName" : return driver.findElementByTagName(locValue);
			case "partialLinkText": return driver.findElementByPartialLinkText(locValue);
			case "cssSelector": return driver.findElementByCssSelector(locValue);
			}
			reportStep("pass", "The element "+locator+" and "+locValue+" located successfully");
			
		} catch (NoSuchElementException e) {
			//System.out.println("The Element Is Not Located ");
			reportStep("fail", "The element "+locator+" and "+locValue+" not located ");
		}
		return null;
	}
	
	public List<WebElement> locateElements(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementsById(locValue);
			case "class": return driver.findElementsByClassName(locValue);
			case "xpath": return driver.findElementsByXPath(locValue);
			case "name": return driver.findElementsByName(locValue);
			case "linkText": return driver.findElementsByLinkText(locValue);
			case "tagName" : return driver.findElementsByTagName(locValue);
			case "partialLinkText": return driver.findElementsByPartialLinkText(locValue);
			case "cssSelector": return driver.findElementsByCssSelector(locValue);
			}
			reportStep("pass", "The element "+locator+" and "+locValue+" located successfully");
		}
		catch (NoSuchElementException e) {
			//System.out.println("The Element Is Not Located ");
			reportStep("fail", "The element "+locator+" and "+locValue+" not located ");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		try {
			return driver.findElementById(locValue);
		} catch (NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
		}
		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportStep("pass", "The Data "+data+" is Entered successfully  ");
			takeSnap();
		} catch (Exception e) {
			reportStep("fail", "The Data "+data+" is not Entered ");
			takeSnap();
		}
	}

	@Override
	public void click(WebElement ele) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			//System.out.println("The Element "+ele+" Clicked Successfully");
			reportStep("pass","The Element "+ele+" Clicked Successfully");
			takeSnap();
		} catch (Exception e) {
			reportStep("fail", "The Element "+ele+" not Clicked ");
			takeSnap();
		}
	}
	public void clickWithOutSnap(WebElement ele) {
		String text = ele.getText();
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			//System.out.println("The Element "+ele+" Clicked Successfully");
			reportStep("pass","The Element "+text+" Clicked Successfully");
			
		} catch (Exception e) {
			reportStep("fail", e.getMessage());
			
		}
		
	}
	@Override
	public String getText(WebElement ele) {
		try {
			 ele.getText();
			 reportStep("pass", "the data get from"+ele+"successfully");
		} catch (Exception e) {
			 reportStep("fail", "the data is not get from"+ele);
		}
		return null;
		
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			//System.out.println("The DropDown Is Selected with "+value);
			reportStep("pass", "The DropDown Is Selected with "+value);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//System.out.println("The DropDown Is Not Selected with "+value);
			reportStep("fail", "The DropDown Is not Selected with "+value);
		}
		finally {
			takeSnap();
		}
		
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
	
		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);
			//System.out.println("The DropDown Is Selected with "+index);
			reportStep("pass", "The DropDown Is Selected with "+index);
		} catch (Exception e) {
			
			//System.out.println("The DropDown Is Not Selected with "+index);
			reportStep("fail", "The DropDown Is not Selected with "+index);
		}
		finally {
			takeSnap();
		}
		
	}
	public void selectDropDownUsingValue(WebElement ele, String value) {
		
		try {
			Select dd = new Select(ele);
			dd.selectByValue(value);
			//System.out.println("The DropDown Is Selected with "+value);
			reportStep("pass", "The DropDown Is Selected with "+value);
		} catch (Exception e) {
		
			//System.out.println("The DropDown Is Not Selected with "+value);
			reportStep("pass", "The DropDown Is not Selected with "+value);
		}
		finally {
			takeSnap();
		}
		
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		
		try {
			String title = driver.getTitle();
			if(title.equals(expectedTitle)) {
				
				reportStep("pass", "AcTittle"+title+"equals ExTittle"+expectedTitle);
			}
			else {
				reportStep("fail", "AcTittle"+title+"NOT equals ExTittle"+expectedTitle);
			}
			
		} catch (Exception e) {
			reportStep("fail", "No Tittle");
		}
		finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOf(ele));
			String text = ele.getText();
			if(text.equals(expectedText)){
				System.out.println("Expected Text"+(expectedText)+"is equal to Actual Text"+text);
			}
			else {
				System.out.println("Expected Text"+(expectedText)+"is NotEqual to Actual Text"+text);
			}
		} catch (Exception e) {
			reportStep("fail", e.getMessage());
		}
		finally
		{
			takeSnap();
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			String text = ele.getText();
			if(text.contains(expectedText)){
				System.out.println("");
			}
			else {
				System.out.println("Expected Text"+(expectedText)+"is NotEqual to Actual Text"+text);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			List<String> listOfWindow = new ArrayList<String>();
			listOfWindow.addAll(allWindows);
			driver.switchTo().window(listOfWindow.get(index));
			//System.out.println("The Window is Switched ");
			reportStep("pass", "The Window is Switched to"+index );
		} catch (Exception e) {
			reportStep("fail", "The Window Not Switched to"+index );
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			reportStep("pass", "Frame Switched to"+ele);
		} catch (Exception e) {
			
			reportStep("fail", "Frame Not Switched to"+ele);
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		
	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();
		
	}

	@Override
	public String getAlertText() {
		return driver.switchTo().alert().getText();
		
	}

	@Override
	public long takeSnap() {

		long number = (long) Math.floor(Math.random() * 900000000L) + 10000000L; 
		try {
			FileUtils.copyFile(driver.getScreenshotAs(OutputType.FILE) , new File("./reports/images/"+number+".jpg"));
		} catch (WebDriverException e) {
			System.out.println("The browser has been closed.");
		} catch (IOException e) {
			System.out.println("The snapshot could not be taken");
		}
		return number;
		/*try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
			//reportStep("pass", "taking screen shot successfully");
		} catch (IOException e) {
			//reportStep("fail", "Not taking screen shot");
		}
		i++;*/
	}

	@Override
	public void closeBrowser() {
		try {
			driver.close();
			reportStep("pass", "Browser Closed");
		} catch (Exception e) {
			reportStep("fail", "Browser Not Closed");
		}

	}

	@Override
	public void closeAllBrowsers() {
		try {
			driver.quit();
			reportStep("pass", "Browser quit");
		} catch (Exception e) {
			reportStep("fail", "Browser Not quit");
		}
		

	}
	
	public void clear(WebElement ele) {
	try {
		ele.clear();
		reportStep("pass", ele+"data cleared successfully");
	} catch (Exception e) {
		
		reportStep("fail", e.getMessage());
	}
	}

}
