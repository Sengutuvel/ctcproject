package utils;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public abstract class Report {
	public static ExtentTest logger ;
	public String testCaseName, testDesc, category , author;
	public static ExtentHtmlReporter html ;
	public static ExtentReports extent;
//@BeforeSuite
	public void startResult() {
		html = new ExtentHtmlReporter("./reports/extentresults.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}
	public abstract long takeSnap();
	
	public void reportStep(String status, String desc) {
		MediaEntityModelProvider img = null;
		boolean bSnap = true;
		if(bSnap && !status.equalsIgnoreCase("INFO")){

			long snapNumber = 100000L;
			snapNumber = takeSnap();
			try {
				img = MediaEntityBuilder.createScreenCaptureFromPath
						("./../reports/images/"+snapNumber+".jpg").build();
			} catch (IOException e) {				
			}
		}
		if(status.equalsIgnoreCase("PASS")) {
			logger.pass(desc, img);			
		}else if (status.equalsIgnoreCase("FAIL")) {
			logger.fail(desc, img);
			throw new RuntimeException();
		}else if (status.equalsIgnoreCase("WARNING")) {
			logger.warning(desc, img);
		}else if (status.equalsIgnoreCase("INFO")) {
			logger.info(desc);
		}
		/*if (status.equalsIgnoreCase("pass")) {
			logger.log(Status.PASS, desc);
			
		} else if (status.equalsIgnoreCase("fail")) {
			logger.log(Status.FAIL, desc);			
		}*/
	}
	//@AfterSuite
	public void endResult() {
		extent.flush();
	}
	
	
	//@BeforeMethod
	public void beforeMethod() {
		logger = extent.createTest(testCaseName, testDesc);
		logger.assignAuthor(author);
		logger.assignCategory(category);		
	}
	
	
	
	
	
	
	
	
	
}
