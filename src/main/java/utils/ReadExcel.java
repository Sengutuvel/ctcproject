package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcel {
	//@Test
	public static Object[][] readExcel(String dataSheetName) throws IOException {
		
		XSSFWorkbook wb = new XSSFWorkbook("./data/"+dataSheetName+".xlsx");
		XSSFSheet sheet = wb.getSheetAt(0);
		int lastRowNum = sheet.getLastRowNum();
		System.out.println(lastRowNum);
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		System.out.println(lastCellNum);
		Object[][] data = new Object[lastRowNum][lastCellNum];
		
		for (int i = 1; i <= lastRowNum; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < lastCellNum; j++) {
				try {
					String cellvalue = row.getCell(j).getStringCellValue();
					 data[i-1][j] = cellvalue;
					
					System.out.println(cellvalue);
				} catch (NullPointerException e) {
					System.out.println("");
				}
			} 
		}
		return data;
		
		
		
		
		
		
	}
	

}
