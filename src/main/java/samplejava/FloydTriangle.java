package samplejava;

import java.util.Scanner;

public class FloydTriangle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the No of rows you want to draw a floyd triangle");
		int row = sc.nextInt();
		int count = 1;
		for (int i = 1; i <=row; i++) {
			
			for (int j = 1; j <= i; j++) {
				
				System.out.print(count+" ");
				count++;
				
			}
			System.out.println();
			
		}
		
		
	}

}
