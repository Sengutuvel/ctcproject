package samplejava;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collections;

import javax.imageio.ImageIO;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MoveAction;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class JavascriptExecutorclass {

	public static void main(String[] args) throws HeadlessException, AWTException, IOException {
		// TODO Auto-generated method stub

		WebDriver driver = new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps");
		WebElement username = driver.findElement(By.id("username"));
		WebElement login = driver.findElement(By.className("decorativeSubmit"));
		String cssValue = username.getCssValue("backgroundColor");
		System.out.println(cssValue);
		JavascriptExecutor js = ((JavascriptExecutor)driver);
		//highilighted code
		js.executeScript("arguments[0].style.border='3px solid red'", username);
		//backgroundcolor changing
		js.executeScript("arguments[0].style.backgroundColor='"+cssValue+"'",login);
		//create alert
		js.executeScript("alert('ther is an alert button')");
		
		Actions action = new Actions(driver);
		
		action.clickAndHold(login).moveByOffset(300, 500).release().perform();
		Select sc = new Select(login);
		sc.selectByIndex(1);
		
		driver.switchTo();
		action.dragAndDropBy(username, 500, 50).release().perform();
		WebElement element = driver.findElement(By.xpath("//input[@type=\"button\"]"));
	    // Trigger the alert
	    element.click();
	    BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
	    ImageIO.write(image, "png", new File("c:\\localdev\\bla.png"));
	    driver.switchTo().alert().accept();
		
		
		
		
	}

}
