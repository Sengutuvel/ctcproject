package samplejava;

public class FibbinociSeries {
public static void main(String[] args) {
	

	int num = 13;
	int t1 = 0;
	int t2 = 1;
	
	for (int i = 1; i <= num; i++) {
		
		System.out.print(t1+",");
		
		int sum = t2 +t1;
		t1 = t2;
		t2 = sum;
	}
	
}
}

