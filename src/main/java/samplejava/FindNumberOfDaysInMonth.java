package samplejava;

import java.util.Scanner;

import org.testng.annotations.Test;

public class FindNumberOfDaysInMonth {

	int Numberofdays = 0;
	String monthname = "";
	
	@Test
	public void find() {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the month");
		int month = sc.nextInt();
		
		System.out.println("Enter the year");
		int year = sc.nextInt();
		
		switch (month) {
		case 1:
			Numberofdays = 31;
			monthname = "January";
			break;
		case 2:
			if((year%400==0)||((year%4==0)&&(year%100!=0))) {
				Numberofdays = 29;
				monthname = "february";
			}
			else {
			
			Numberofdays = 28;
			monthname = "february";}
			break;
		case 3:
			Numberofdays = 31;
			monthname = "march";
			break;
		case 4:
			Numberofdays = 30;
			monthname = "april";
			break;
		case 5:
			Numberofdays = 31;
			monthname = "may";
			break;
		case 6:
			Numberofdays = 30;
			monthname = "june";
			break;
		case 7:
			Numberofdays = 31;
			monthname = "july";
			break;
		case 8:
			Numberofdays = 30;
			monthname = "augest";
			break;
		case 9:
			Numberofdays = 31;
			monthname = "september";
			break;
		case 10:
			Numberofdays = 30;
			monthname = "october";
			break;
		case 11:
			Numberofdays = 31;
			monthname = "november";
			break;
		case 12:
			Numberofdays = 30;
			monthname = "december";
			break;

		default:
			System.out.println("Not a Valid month");
			break;
		}
		
		System.out.println(monthname+"-"+year+" has "+Numberofdays+" days");
	}
}
