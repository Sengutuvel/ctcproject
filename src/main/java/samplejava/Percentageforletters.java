package samplejava;

public class Percentageforletters {

	
	public static void main(String[] args) {
		int dig = 0,up = 0,lo = 0,spe = 0;
		
		String str = "sdfghjklkjhgfd7412369852S####";
		
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if(Character.isDigit(c)) {
				dig++;
			}
			else if(Character.isUpperCase(c)) {
				up++;
			}
			else if(Character.isLowerCase(c)) {
				lo++;
			}
			else{
				spe++;
			}
		}
		
		double n = dig*100/str.length();
		double n1 = up*100/str.length();
		double n2 = lo*100/str.length();
		double n3 = spe*100/str.length();
		
		System.out.println("number percentage is "+ n);
		System.out.println("uppercase percentage is "+ n1);
		System.out.println("lowercase percentage is "+ n2);
		System.out.println("special percentage is "+ n3);
	}
}
