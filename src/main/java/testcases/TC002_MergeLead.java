package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.FindLeadsPage;
import pages.LoginPage;
import pages.MergeLeadsPage;
import wdMethods.Projectmethods;

public class TC002_MergeLead extends Projectmethods {

@BeforeTest(groups= {"any"})
	public void setData() {
		testCaseName = "TC002_MergeLead";
		testDesc = "Merging two lead";
		category = "smoke";
		author = "sengutuvel";
		dataSheetName = "mergelead";
	}
	
@Test(dataProvider="qa")
	public void mergeLead(String usrname,String password,String firstname,String secondname) throws InterruptedException {
		
		new LoginPage()
		.enterUserName(usrname)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickMergeLead()
		.clickPartyIdFromimg()
		
		.enterFirstName(firstname)
		.clickFindLeadButton()
		.clickFirstLeadIdSelection()
	
		.clickPartyIdToimg()
	
		.enterFirstName(secondname)
		.clickFindLeadButton()
		.clickFirstLeadIdSelection()
		.clickMergeButton()
		.acceptAlert();
		
		
		
	}
}
