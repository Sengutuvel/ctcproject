package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.Projectmethods;

public class TC001_CreateLead extends Projectmethods {

@BeforeTest(groups= {"any"})
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDesc = "Creating lead";
		category = "smoke";
		author = "sengutuvel";
		dataSheetName = "createlead";
	}
	
	@Test(dataProvider = "qa")
	public void createLead(String usrname,String password,String cpName,String fName,String lName,String ph,String mail) {
		
		new LoginPage()
		.enterUserName(usrname)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.entercompanyName(cpName)
		.entefirstName(fName)
		.entelastName(lName)
		.entePNumber(ph)
		.enteEmail(mail)
		.clickCreateLeadbutton()
		.verifyFirstName();
		
	}
}
