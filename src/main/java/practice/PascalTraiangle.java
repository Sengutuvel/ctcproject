package practice;

public class PascalTraiangle {
	
	public static void main(String[] args) {
		
		int input = 5;
		int spaces = input;
		for (int i = 0; i < input; i++) {
			
			for (int s = 1; s <spaces; s++) {
				System.out.print(" ");
			}
			int number = 1;
			for (int j = 0; j <= i; j++) {
				System.out.print(number+" ");
				number = number* (i-j)/(j+1);
			
			}
			spaces--;
			System.out.println();
			
		}
	}

}
