package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@CucumberOptions(
		features = "src/test/java/features/CreateLead2.feature",
		glue = {"CreateLeadIMPL","pages"},
		monochrome = true
		//tags = "@smoke"
		
		//dryRun = true,
		//snippets = SnippetType.CAMELCASE
		
		)
@RunWith(Cucumber.class)
public class RunCreateLead2 {

}
