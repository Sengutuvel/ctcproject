package CreateLeadIMPL;

import cucumber.api.Result.Type;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.Projectmethods;

public class Hooks extends Projectmethods {
	//String testCaseName;
	//String testDesc;
@Before
public void beforescenario(Scenario sc) {
	startResult();
 testCaseName = "createleadwithcucumber";
 testDesc = "creating lead number";
 author = "sengutuvel";
 category = "SmokeTest";
	beforeMethod();
	startApp("chrome", "http://leaftaps.com/opentaps");
	
	String name = sc.getName();
	System.out.println(name);
	String id = sc.getId();
	System.out.println(id);
	
	
}

@After
public void afterscenarion(Scenario sc) {
	
	Type status = sc.getStatus();
	System.out.println(status);
	closeBrowser();
	endResult();
	
}

}
