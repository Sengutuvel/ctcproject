/*package CreateLeadIMPL;

import java.sql.Driver;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class createleadimplement {
	public static ChromeDriver driver;
	@Given("launch the browser")
	public void launthebrowser() {
	    // Write code here that turns the phrase above into concrete actions
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
	   
	}

	
	@And("enter the url")
	public void enterTheUrl() {
	    // Write code here that turns the phrase above into concrete actions
		driver.get("http://leaftaps.com/opentaps");
		
	    
	}

	@Given("enter the username as (.*)")
	public void enterTheUsername(String username) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("username").sendKeys(username);
		
	   
	}

	@Given("enter the password as (.*)")
	public void enterThePassword(String password) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("password").sendKeys(password);
	    
	}

	@Given("click the loging button")
	public void clickTheLogingButton() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementByClassName("decorativeSubmit").click();
	    
	}

	@Given("click the CRM link")
	public void clickTheCRMLink() {
	    // Write code here that turns the phrase above into concrete actions
		
		driver.findElementByLinkText("CRM/SFA").click();
	   
	}

	@Given("click the leads tab")
	public void clickTheLeadsTab() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Leads").click();
	    
	}

	@Given("click the create lead button")
	public void clickTheCreateLeadButton() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Create Lead").click();
	   
	}

	@Given("enter the first name as (.*)")
	public void enterTheFirstName(String fname) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	   
	}

	@Given("enter the second name as (.*)")
	public void enterTheSecondName(String lname) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);

	}

	@Given("enter the company name as (.*)")
	public void enterTheCompanyName(String compname) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_companyName").sendKeys(compname);
	    
	}

	@When("click the createleaad button")
	public void clickTheCreateleaadButton() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementByXPath("//input[@value='Create Lead']").click();
	    
	}

	@Then("verify the create lead success")
	public void verifyTheCreateLeadSuccess() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Create lead success");
	    
	}


}
*/